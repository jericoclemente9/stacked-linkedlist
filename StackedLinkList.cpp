#include<iostream>
#include<cstdlib>
#include<stdio.h>

using namespace std;

class List{
	private:
		typedef struct node{
			int num;
			string title;
			string singer;
			node* next;
			
		}*nodePtr;
		
		nodePtr head;
		nodePtr curr;
		nodePtr temp;
		nodePtr top;
	
	public:
		List();
		void Push(string addTitle, string addSinger);
		void Pop();
		void Display();
		string DisplayTop();
};

List::List()
{
	head=NULL;
	curr=NULL;
	temp=NULL;
	top = NULL;
}
void List::Push(string addTitle, string addSinger)
{
	nodePtr n = new node;
	n->title = addTitle;
	n->singer = addSinger;
	n->next = NULL;
	if(head!=NULL){
		curr=head;
		while(curr->next!=NULL){
			curr = curr->next;
		}
		curr->next = n;
		n->num=top->num+1;
		curr=curr->next;
	}
	else{
		head=n;
		n->num=1;
	}
	top = n;
}

void List::Pop()
{
	curr = head;
	temp = head;
	if (head==NULL){
		cout<<"Nothing to delete"<<endl;
	}
	else if(curr->next==NULL){
		head=NULL;
		cout<<"Stack is now empty!\n";
	}
	else{
		while(curr->next!=NULL){
			temp = curr;
			curr = curr->next;
		}
		top = temp;
		top->next=NULL;
		cout<<"Last added item has been removed\n";
	}
}

void List::Display()
{
	if(head!=NULL){
		for(int i = top->num;i>0;i--)
		{
			curr=head;
			while(curr!=NULL&&curr->num!=i)
			{
				curr = curr->next;
			}
			cout << curr->num << ". Title: " << curr->title <<" by: "<<curr->singer<< endl<<endl;
		}
	}
	else{
		cout<<"Nothing to display\n";
	}
}


List list;

void switcher()
{
	cout<<"1. Push 2. Pop 3. Display \n";
	cout<<"Enter choice: ";
	char c;
	cin>>c;
	cin.ignore(1,'\n');
	switch (c){
		case '1':{
			string addTitle, addSinger;
			cout<<"Enter Title: ";
			getline(cin,addTitle);
			cout<<"Enter Singer: ";
			getline(cin,addSinger);
			list.Push(addTitle, addSinger);
			cout<<"Music Saved!\n";
			break;
		}
		case '2':{
			list.Pop();
			break;
		}
		case '3':{
			list.Display();
			break;
		}
		default:{
			cout<<"Invalid input!\n";
			break;
		}
	}
	switcher();
}


int main()
{
	cout<<"*****************************************************************\n";
	cout<<"*   ###### #########   ##    ##### #   #   ####### #######  #   *\n";
	cout<<"*   #          #      #  #  #      #  #       #       #     #   *\n";
	cout<<"*   ######     #     # ## # #      ###        #       #     #   *\n";
	cout<<"*        #     #     #    # #      #  #       #       #         *\n";
	cout<<"*   ######     #     #    #  ##### #   #   #######    #     #   *\n";
	cout<<"*****************************************************************\n";
	switcher();
}
